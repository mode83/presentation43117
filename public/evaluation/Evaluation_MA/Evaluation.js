var navbarElt= document.getElementById("menu_horizontal");
var bouton_saviez_vous= document.getElementById("saviez_vous");
var bouton_plusdinfo1= document.getElementById("plus_dinfo1");
var bouton_plusdinfo2= document.getElementById("plus_dinfo2");
var bouton_plusdinfo3= document.getElementById("plus_dinfo3");
var bouton_cliquez= document.getElementById("cliquez");
var bouton_decouvrez= document.getElementById("bouton_section4");

var croix = document.getElementById("croix");
var popup = document.getElementById("pop");

var listeliens=document.querySelectorAll("#menu_horizontal li a");

     /******************navbar***************/

window.addEventListener("scroll",function(){
if (document.documentElement.scrollTop >=50)
{
      navbarElt.setAttribute("class","navbar_opac");
      navbarElt.style.color="white";
      for (var i = 0; i <listeliens.length ; i++)
       {
      	listeliens[i].classList.remove("lien_noir");
       }
  }else
   {
      navbarElt.classList.remove("navbar_opac");
      for (var i = 0; i <listeliens.length ; i++)
       {
      	listeliens[i].classList.add("lien_noir");
       }
    }
});

        /******************3boutton plus info et saviez_vous***************/

  bouton_saviez_vous.addEventListener("mouseover",function(){
  bouton_saviez_vous.style.backgroundColor='#FFF';
  bouton_saviez_vous.style.color='#ff0066';
  bouton_saviez_vous.style.border='solid 1px #ff0066';

});
  bouton_saviez_vous.addEventListener("mouseout",function(){
  bouton_saviez_vous.style.backgroundColor='#ff0066';
  bouton_saviez_vous.style.color='#fff';
});


  bouton_plusdinfo1.addEventListener("mouseover",function(){
  bouton_plusdinfo1.style.border='solid 2px #ff0066';
  bouton_plusdinfo1.style.color='#ff0066';
});
  bouton_plusdinfo1.addEventListener("mouseout",function(){
    bouton_plusdinfo1.style.border='solid 1px #000';
    bouton_plusdinfo1.style.color='#000';
});


  bouton_plusdinfo2.addEventListener("mouseover",function(){
  bouton_plusdinfo2.style.border='solid 2px #ff0066';
  bouton_plusdinfo2.style.color='#ff0066';
});
  bouton_plusdinfo2.addEventListener("mouseout",function(){
    bouton_plusdinfo2.style.border='solid 1px #000';
    bouton_plusdinfo2.style.color='#000';
});

  bouton_plusdinfo3.addEventListener("mouseover",function(){
  bouton_plusdinfo3.style.border='solid 2px #ff0066';
  bouton_plusdinfo3.style.color='#ff0066';
});
  bouton_plusdinfo3.addEventListener("mouseout",function(){
  bouton_plusdinfo3.style.border='solid 1px #000';
  bouton_plusdinfo3.style.color='#000';
});

bouton_cliquez.addEventListener("mouseover",function(){
bouton_cliquez.style.backgroundColor='#FFF';
bouton_cliquez.style.color='#ff0066';
bouton_cliquez.style.border='solid 1px #ff0066';

});
bouton_cliquez.addEventListener("mouseout",function(){
bouton_cliquez.style.backgroundColor='#ff0066';
bouton_cliquez.style.color='#fff';
});


bouton_decouvrez.addEventListener("mouseover",function(){
bouton_decouvrez.style.backgroundColor='#FFF';
bouton_decouvrez.style.color='#ff0066';
bouton_decouvrez.style.border='solid 1px #ff0066';

});
bouton_decouvrez.addEventListener("mouseout",function(){
bouton_decouvrez.style.backgroundColor='#ff0066';
bouton_decouvrez.style.color='#fff';
});



/******************POPUP en cliquant sur saviez_vous***************/

bouton_saviez_vous.addEventListener("click",function(){
    popup_contact();
    popup.style.top=document.documentElement.scrollTop+"px";

});

croix.addEventListener("click",fermer);
popup.addEventListener("click",fermer);

function popup_contact()
{
    if (popup.classList.contains("hide"))
    {
        popup.classList.remove("hide");
        popup.classList.add("show");
    }
}
function fermer(e)
{
  if(e.target.id !='pop_tel' && e.target.id !='tel')
  {
    if (popup.classList.contains("show"))
    {
        popup.classList.remove("show");
        popup.classList.add("hide");
    }
  }
}

/******************les sous menu de la navbar***************/

var menu_siteonglets= document.getElementById("menu_siteonglets");
var menu_tout= document.getElementById("menu_tout");


var site_onglets = document.getElementById("sous_menu1");
var tout= document.getElementById("sous_menu2");


site_onglets.addEventListener("click",function(){
    if (menu_siteonglets.classList.contains("show"))
    {
        menu_siteonglets.classList.remove("show");
        menu_siteonglets.classList.add("hide");
    }
    else
    {
        menu_siteonglets.setAttribute("class","show");

    }
});

tout.addEventListener("click",function(){
    if (menu_tout.classList.contains("show"))
    {
        menu_tout.classList.remove("show");
        menu_tout.classList.add("hide");
    }
    else
    {
        menu_tout.setAttribute("class","show");

    }
});
