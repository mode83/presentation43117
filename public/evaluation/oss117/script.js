// Pour faire apparaitre la section 1 grâce à la flèche
var arrow_down = document.getElementById('arrow_down');
var section1 = document.getElementById('section1');

arrow_down.addEventListener('click', function(){ // On ajoute un listener au clic sur "arrow_down" (flèche avec le cercle rose)
  section1.classList.remove('hide');             // On change la classe qui permet l'affichage de la section1
  section1.classList.add('show');
});


// Affichage de la barre de navigation en fonction du scroll
var nav = document.getElementById('navbar');

window.addEventListener('scroll', scrollNavbar);

function scrollNavbar() {
  if (document.documentElement.scrollTop >= 50) // si on scroll au dela de 50px
  {
    nav.setAttribute('class', 'nav_scrolling'); // on attribue la classe "nav_scrolling"
  } else {
    nav.classList.remove('nav_scrolling'); // Sinon on enlève la classe "nav_scrolling"
  }
}

//bouton pop up
var btn = document.getElementById('btn-saviez-vous');
var modal = document.getElementById('modal');

btn.addEventListener('click', showOrHideModal)
window.addEventListener('click', closePopup);
window.addEventListener('keypress', echap_close_popup);
function showOrHideModal(){
  if (modal.classList.contains('hide')){
    modal.setAttribute('class', 'show');
  } else {
    modal.setAttribute('class', 'hide');
  }
}

function closePopup(e){
  if (e.target.id === "close" || e.target.id === "modal"){
    modal.setAttribute('class', 'hide');
  }
}
function echap_close_popup(e){
  if (e.keyCode == 27){
    modal.setAttribute('class', 'hide');
  }
}


//Dropdown menu "Site onglets"
/*
onglet.addEventListener('click',function(e){
  if (ongletDrop.classList.contains('hide'))
    {
      ongletDrop.classList.remove('hide');
      ongletDrop.classList.add('show');
    } else if (ongletDrop.classList.contains('show'))
    {
      ongletDrop.classList.remove('show');
      ongletDrop.classList.add('hide');
    }
});
*/

//Dropdown menu "tout" et "onglet"

var tout = document.getElementById('tout');
var toutDrop = document.getElementsByClassName('drop-tout-content')[0];
var onglet = document.getElementById('onglet');
var ongletDrop = document.getElementsByClassName('drop-onglet-content')[0];


window.addEventListener('click',function(e){
console.log(e.target.id);
  if (e.target.id === 'tout')
  {
    if (toutDrop.classList.contains('hide'))
      {
        toutDrop.classList.remove('hide');
        toutDrop.classList.add('show');
      } else if (toutDrop.classList.contains('show'))
        {
          toutDrop.classList.remove('show');
          toutDrop.classList.add('hide');
        }
  } else if (e.target.id === 'onglet')
    {
      if (ongletDrop.classList.contains('hide'))
        {
          ongletDrop.classList.remove('hide');
          ongletDrop.classList.add('show');
        } else if (ongletDrop.classList.contains('show'))
        {
          ongletDrop.classList.remove('show');
          ongletDrop.classList.add('hide');
        }
    } else {
      toutDrop.classList.remove('show');
      toutDrop.classList.add('hide');
      ongletDrop.classList.remove('show');
      ongletDrop.classList.add('hide');
    }
});
