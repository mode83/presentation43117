<?php 
	 
	include ('core/connection.php');
	include ('functions/main.php');
	if(isset($_SESSION['login'])===false){
		header('Location: index.php');
	}else{
	//Vérifie si l'administrateur peut poster l'article
 	if($_POST){
		$ptitle = $_POST['post_title'];
		$pdecscription = $_POST['post_decscription'];
		 
		$pdate = date("Y-m-d H:i:s");
 	if(empty($ptitle) or empty($pdecscription)){
			$errors = '<div class="error2"><p> Tous les champs sont requis. Veuillez réessayer.</p></div>';
		}else{
			//On verifie si l'image est choisie
			if (isset($_FILES['post_image'])===true) {		 
				if (empty($_FILES['post_image']['name']) ===true) {
					$errors = '<div class="error2">Veuillez choisir une image.</div>';
				 
				 }else {   
				 	//Verification du format                                                                                                    
					 $allowed = array('jpg','jpeg','gif','png'); 
					 $file_name = $_FILES['post_image']['name']; 
					 $file_extn = strtolower(end(explode('.', $file_name)));
					 $file_temp = $_FILES['post_image']['tmp_name'];
					 
					 if (in_array($file_extn, $allowed)===true) {
					 		//renome l'image dans un dossier du répertoire (galerie)
							$file_parh = 'galerie/' . substr(md5(time()), 0, 10).'.'.$file_extn;
							//move image to our image folder
							move_uploaded_file($file_temp, $file_parh);						 	
							$query = $pdo->prepare("INSERT INTO `galerie` (`id`, `post_title`, `post_decscription`, `post_date`, `post_image`) VALUES (NULL, ?, ?, ?, ?)");
							$query->bindValue(1, $ptitle);	
							$query->bindValue(2, $pdecscription);	
						
							$query->bindValue(3, $pdate);	
							$query->bindValue(4, $file_parh);
 							$query->execute();	
							header('Location: galerie.php');	

					 }
				 	}
				}
						

		}



		
	}
	
?>

	
	
		
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Le Bateau de Yûna - Ajouter une image à la galerie</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />

  
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,600,400italic,700' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="../css/animate.css">
	<!-- Flexslider -->
	<link rel="stylesheet" href="../css/flexslider.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="../css/icomoon.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="../css/magnific-popup.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="../css/bootstrap.css">

	<!-- 
	Default Theme Style 
	You can change the style.css (default color purple) to one of these styles
	-->
	<link rel="stylesheet" href="../css/style.css">

	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../header/styles.css">
	<!-- Styleswitcher ( This style is for demo purposes only, you may delete this anytime. ) -->
	<link rel="stylesheet" id="theme-switch" href="../css/style.css">
	<!-- End demo purposes only -->


	<style>
	/* For demo purpose only */

	/*
	GREEN
	8dc63f
	RED
	FA5555
	TURQUOISE
	27E1CE
	BLUE 
	2772DB
	ORANGE
	FF7844
	YELLOW
	FCDA05
	PINK
	F64662
	PURPLE
	7045FF

	*/
	
	/* For Demo Purposes Only ( You can delete this anytime :-) */
	#colour-variations {
		padding: 10px;
		-webkit-transition: 0.5s;
	  	-o-transition: 0.5s;
	  	transition: 0.5s;
		width: 140px;
		position: fixed;
		left: 0;
		top: 100px;
		z-index: 999999;
		background: #fff;
		/*border-radius: 4px;*/
		border-top-right-radius: 4px;
		border-bottom-right-radius: 4px;
		-webkit-box-shadow: 0 0 9px 0 rgba(0,0,0,.1);
		-moz-box-shadow: 0 0 9px 0 rgba(0,0,0,.1);
		-ms-box-shadow: 0 0 9px 0 rgba(0,0,0,.1);
		box-shadow: 0 0 9px 0 rgba(0,0,0,.1);
	}
	#colour-variations.sleep {
		margin-left: -140px;
	}
	#colour-variations h3 {
		text-align: center;;
		font-size: 11px;
		letter-spacing: 2px;
		text-transform: uppercase;
		color: #777;
		margin: 0 0 10px 0;
		padding: 0;;
	}

	#colour-variations ul,
	#colour-variations ul li {
		padding: 0;
		margin: 0;
	}
	#colour-variations ul {
		margin-bottom: 20px;
		float: left;	
	}
	#colour-variations li {
		list-style: none;
		display: inline;
	}
	#colour-variations li a {
		width: 20px;
		height: 20px;
		position: relative;
		float: left;
		margin: 5px;
	}


 
	

	#colour-variations a[data-layout="boxed"],
	#colour-variations a[data-layout="wide"] {
		padding: 2px 0;
		width: 48%;
		border: 1px solid #ededed;
		text-align: center;
		color: #777;
		display: block;
	}
	#colour-variations a[data-layout="boxed"]:hover,
	#colour-variations a[data-layout="wide"]:hover {
		border: 1px solid #777;
	}
	#colour-variations a[data-layout="boxed"] {
		float: left;
	}
	#colour-variations a[data-layout="wide"] {
		float: right;
	}

	.option-toggle {
		position: absolute;
		right: 0;
		top: 0;
		margin-top: 5px;
		margin-right: -30px;
		width: 30px;
		height: 30px;
		background: #8dc63f;
		text-align: center;
		border-top-right-radius: 4px;
		border-bottom-right-radius: 4px;
		color: #fff;
		cursor: pointer;
		-webkit-box-shadow: 0 0 9px 0 rgba(0,0,0,.1);
		-moz-box-shadow: 0 0 9px 0 rgba(0,0,0,.1);
		-ms-box-shadow: 0 0 9px 0 rgba(0,0,0,.1);
		box-shadow: 0 0 9px 0 rgba(0,0,0,.1);
	}
	.option-toggle i {
		top: 2px;
		position: relative;
	}
	.option-toggle:hover, .option-toggle:focus, .option-toggle:active {
		color:  #fff;
		text-decoration: none;
		outline: none;
	}

	</style>

	<link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet"> 
	<!-- End demo purposes only -->


	<!-- Modernizr JS -->
	<script src="../js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="../css/style.css">
	<script type="text/javascript" src="./js/jquery.js"></script>
	   <script src="http://cdn.ckeditor.com/4.5.9/full/ckeditor.js"></script>	
	   </head>


	<!-- 
		INFO:
		Add 'boxed' class to body element to make the layout as boxed style.
		Example: 
		<body class="boxed">	
	-->
	<body>
	
	<?PHP include("../header/header1.php"); ?>
		<!-- #fh5co-header -->

		<!-- END #fh5co-hero -->

		<section id="fh5co-pricing">
			<div class="container">
				<div class="row">
			 
				 
			 <center>
				<div class="animate-box" style="width: 50%;">
					<div class="price-box ">
						 
						<h2 class="pricing-plan">Ajouter une image à la galerie</h2>
						
						<hr>
						<ul class="pricing-info">
							  
				<table>
					<tbody>
						 
						 
					
			<form action="" method="post" enctype="multipart/form-data">
			 <?php if(isset($errors)){
						echo $errors;
					}?>
				<h2 class="pricing-plan" align="left"><b>Titre :</b></h2>
				<input align="left" name="post_title" class="form-control" type="text"></input> 
			 
					<hr>
			 		
					<h2 class="pricing-plan" align="left"><b>Description : </b></h2>   
					<textarea rows="3" style="max-width: 100%;" class="form-control" name="post_decscription"></textarea></li>
						
						<hr> 
														<div class="content">
		
			
						<hr>  
			<div class="right-side">
				<div class="right-menu">
					<ul>
					
					</ul>
						
					<hr>
					 
					
						 <h2 class="pricing-plan" align="left"><b>Image</b></h2>
						<input type='file' class="btn btn-default" class="form-control" name="post_image" id="imgInput" /></li>
						<li><img id="preview" src="#"/></li>
						 <li><input class="btn btn-default" value="Valider l'article" type="submit"></input></li>
					
						</form>
				</div>
				<script>
				//replace texarea with editor
          		  CKEDITOR.replace( 'editor' );
          		  function readURL(input) {
          		  	//image preview function
				    if (input.files && input.files[0]) {
				        var reader = new FileReader();

				        reader.onload = function (e) {
				            $('#preview').attr('src', e.target.result);
				        }

				        reader.readAsDataURL(input.files[0]);
				        $('#preview').show();
				    }
				}

					$("#imgInput").change(function(){
					    readURL(this);
					});
        		</script>
			</div>
		</div>
						
					</tbody>
				</table>
			  
			 
					 
						
					</div>
				</div>
				 </center>
				
				
				 
	
				</div>
			</div>
		</section>

		<!-- END #fh5co-pricing -->

		 
		 
		<!-- END #fh5co-testimonials -->

		<section id="fh5co-subscribe">
			<div class="container">
		
			 
			

			</div>
		</section>
		<!-- END #fh5co-subscribe -->
<footer id="fh5co-footer">
			<div class="container">
				<div class="row row-bottom-padded-md">
					<div class="col-md-3 col-sm-6 col-xs-12 animate-box">
						<div class="fh5co-footer-widget">
							<h3>À Propos</h3>
							<ul class="fh5co-links">
								<li><a href="#">Association</a></li>
								<li><a href="#">Actualité</a></li>
								<li><a href="#"></a></li>
								<li><a href="#">Galerie</a></li>
								<li><a href="#">Administration</a></li>
							</ul>
						</div>
					</div>

					 

					<div class="col-md-3 col-sm-6 col-xs-12 animate-box">
						<div class="fh5co-footer-widget">
							<h3>Nous contacter</h3>
							<p>
								<a href="mailto:#">adresse mail</a> <br>
								Le bateau du Yûna 626 route du Val d'Ardène<br>
								83200 Toulon <br>
								<a href="tel:XXXXXXXXXX">XX.XX.XX.XX.XX</a>
							</p>
						</div>
					</div>

					<div class="col-md-3 col-sm-6 col-xs-12 animate-box">
						<div class="fh5co-footer-widget">
							<h3>Nous rejoindre</h3>
							<ul class="fh5co-social">
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="https://www.facebook.com/fabien.savin.56/"><i class="icon-facebook"></i></a></li>
								<li><a href="#"><i class="icon-instagram"></i></a></li>
								<li><a href="#"><i class="icon-youtube-play"></i></a></li>
							</ul>
						</div>
					</div>

				</div>
				
			</div>
			<div class="fh5co-copyright animate-box">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<p class="fh5co-left"><small>&copy; 2017 Association <a href="index.php">Le bateau de Yûna</a>, association régie par la loi du 1er juillet 1901 et le décret du 16 Août 1901. Tous droits réservés.</small></p>
							<p class="fh5co-right"><small class="fh5co-right">Design par <a href="http://waylines.fr" target="_blank">Anthony</a> et bootstrap.</small></p>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- END #fh5co-footer -->
	</div>
	<!-- END #fh5co-page -->
	
	<!-- For demo purposes Only ( You may delete this anytime :-) -->
	<div id="colour-variations">
		<a class="option-toggle"><i class="icon-gear"></i></a>
		<h3>Couleurs</h3>
		<ul>
			<li><a href="javascript: void(0);" data-theme="style"></a></li>
			<li><a href="javascript: void(0);" data-theme="red"></a></li>
			<li><a href="javascript: void(0);" data-theme="turquoise"></a></li>
			<li><a href="javascript: void(0);" data-theme="blue"></a></li>
			<li><a href="javascript: void(0);" data-theme="orange"></a></li>
			<li><a href="javascript: void(0);" data-theme="yellow"></a></li>
			<li><a href="javascript: void(0);" data-theme="pink"></a></li>
			<li><a href="javascript: void(0);" data-theme="purple"></a></li>
			<li><a href="javascript: void(0);" data-theme="bluee"></a></li>
		</ul>
		<a href="#" data-layout="boxed">Réduit</a>
		<a href="#" data-layout="wide">Grand</a>
	</div>
	<!-- End demo purposes only -->

	
	<!-- jQuery -->
	<script src="../js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="../js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="../js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="../js/jquery.waypoints.min.js"></script>
	<!-- Flexslider -->
	<script src="../js/jquery.flexslider-min.js"></script>
	<!-- Magnific Popup -->
	<script src="../js/jquery.magnific-popup.min.js"></script>
	<script src="../js/magnific-popup-options.js"></script>

	<!-- For demo purposes only styleswitcher ( You may delete this anytime ) -->
	<script src="../js/jquery.style.switcher.js"></script>
	<script>
		$(function(){
			$('#colour-variations ul').styleSwitcher({
				defaultThemeId: 'theme-switch',
				hasPreview: false,
				cookie: {
		          	expires: 30,
		          	isManagingLoad: true
		      	}
			});	
			$('.option-toggle').click(function() {
				$('#colour-variations').toggleClass('sleep');
			});
		});
	</script>
	<!-- End demo purposes only -->

	<!-- Main JS (Do not remove) -->
	<script src="../js/main.js"></script>

	<!-- 
	INFO:
	jQuery Cookie for Demo Purposes Only. 
	The code below is to toggle the layout to boxed or wide 
	-->
	<script src="../js/jquery.cookie.js"></script>
	<script>
		$(function(){

			if ( $.cookie('layoutCookie') != '' ) {
				$('body').addClass($.cookie('layoutCookie'));
			}
			
			$('a[data-layout="boxed"]').click(function(event){
				event.preventDefault();
				$.cookie('layoutCookie', 'boxed', { expires: 7, path: '/'});
				$('body').addClass($.cookie('layoutCookie')); // the value is boxed.
			});

			$('a[data-layout="wide"]').click(function(event){
				event.preventDefault();
				$('body').removeClass($.cookie('layoutCookie')); // the value is boxed.
				$.removeCookie('layoutCookie', { path: '/' }); // remove the value of our cookie 'layoutCookie'
			});
		});
	</script>

	</body>
</html>
<?php }  ?>
