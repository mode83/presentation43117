<!doctype html>
<html class="no-js" lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Théâtre - Le Nouveau P'tit ang'art</title>
    
    <meta name="description" content="Le Théâtre Liberté est un lieu culturel pluridisciplinaire au coeur de ville  de Toulon. "> 
     

     
    <meta name="robots" content="index, follow">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <meta property="og:image" content="http://www.theatre-liberte.fr/media/public/img/partage.jpg"/>
    <link rel="image_src" href="http://www.theatre-liberte.fr/media/public/img/partage.jpg" />
    
    <link rel="apple-touch-icon" sizes="57x57" href="http://waylines.fr/logogris.png">
    <link rel="apple-touch-icon" sizes="60x60" href="http://waylines.fr/logogris.png">
    <link rel="apple-touch-icon" sizes="72x72" href="http://waylines.fr/logogris.png">
    <link rel="apple-touch-icon" sizes="76x76" href="http://waylines.fr/logogris.png">
    <link rel="apple-touch-icon" sizes="114x114" href="http://waylines.fr/logogris.png">
    <link rel="apple-touch-icon" sizes="120x120" href="http://waylines.fr/logogris.png">
    <link rel="apple-touch-icon" sizes="144x144" href="http://waylines.fr/logogris.png">
    <link rel="apple-touch-icon" sizes="152x152" href="http://waylines.fr/logogris.png">
    <link rel="apple-touch-icon" sizes="180x180" href="http://waylines.fr/logogris.png">
    <link rel="icon" type="image/png" href="http://waylines.fr/logogris.png" sizes="32x32">
    <link rel="icon" type="image/png" href="http://waylines.fr/logogris.pngg" sizes="194x194">
    <link rel="icon" type="image/png" href="http://waylines.fr/logogris.png" sizes="96x96">
    <link rel="icon" type="image/png" href="http://waylines.fr/logogris.png" sizes="192x192">
    <link rel="icon" type="image/png" href="http://waylines.fr/logogris.png" sizes="16x16">
    <link rel="manifest" href="http://www.theatre-liberte.fr/media/public/img/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="http://www.theatre-liberte.fr/media/public/img/favicons/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="http://www.theatre-liberte.fr/media/public/css/normalize.css">
    <link rel="stylesheet" href="http://www.theatre-liberte.fr/media/public/css/bootstrap.css">
    <link rel="stylesheet" href="http://www.theatre-liberte.fr/media/public/css/main.css">
    <link rel="stylesheet" href="http://www.theatre-liberte.fr/media/public/css/custom.css">

    <link rel="stylesheet" href="http://www.theatre-liberte.fr/media/public/css/correction.css">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <script>window.jQuery || document.write('<script src="http://www.theatre-liberte.fr/media/public/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <!-- Debut covoiturage -->

    <link rel="stylesheet" href="http://www.theatre-liberte.fr/public_page/modules/covoiturage/plugin/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <script type="text/javascript" src="http://www.theatre-liberte.fr/public_page/modules/covoiturage/plugin/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

    <!-- fin Covoiturage -->
<style>
html, body {

font-family: Arial, Georgia, Serif;

}
list {

font-family: Arial, Georgia, Serif;
}
</style>

  </head>
  <body style="font-family: Arial, Georgia, Serif;" class="home">  
    
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Add your site or application content here -->

    <div class="ligneHaut screen"></div>
    
    <div class="menu mob"><a href="javascript:void(0)" class="glyphicon glyphicon-menu-hamburger"></a></div>
    
    <header class="main top">
      <nav class="wrap">

        <a href="javascript:void(0)" class="glyphicon glyphicon-remove mob"></a>

        <form name="Search" method="GET" action="http://www.theatre-liberte.fr/recherche/" onsubmit="return validerFormSearch('Search')">
            <div class="formMoteur screen">  
                    <div class="col-sm-11 col-md-11">
                        <div class="input-group">
                        <input type="text" class="form-control" placeholder="Merci d'indiquer votre recherche ..." name="recherche">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">OK</button>
                        </span>
                        </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->
            </div>
        </form>

        <ul class="list-unstyled saison col-sm-8 col-md-9"> 
          <li class="col-md-3 saison"><a href="http://www.theatre-liberte.fr/la-saison/2015-2016" title="Accéder à la saison">la saison</a></li>
          <li class="col-md-5 creations"><a href="http://www.theatre-liberte.fr/les-creations-du-liberte" title="Accéder aux créations">les créations</a></li>
          <li class="col-md-4 virtuelle"><a href="http://www.theatre-liberte.fr/la-scene-virtuelle" title="Accéder à la scène virtuelle">la scène virtuelle</a></li>
        </ul>

        
        <ul class="list-unstyled pratique col-sm-3 col-md-2">
          <li><a href="http://www.theatre-liberte.fr/informations-pratiques" title="Accéder aux informations pratiques">Informations pratiques</a></li>
          <li><a href="http://www.theatre-liberte.fr/le-theatre-liberte" title="Accéder au Théâtre Liberté">Le Théâtre Liberté</a></li>
          <li><a href="http://www.theatre-liberte.fr/actions" title="Accéder aux actions">Actions !</a></li>
          <li><a href="http://www.theatre-liberte.fr/pour-les-entreprises" title="Accéder à la page pour les entreprises">Pour les entreprises</a></li>
        </ul>

        
        <div class="recherche col-sm-1 col-md-1 screen"><a href="javascript:void(0)" class="moteur"></a></div>
        

      </nav>
    </header>

  <section class="une">


		
	<div class="image screen"></div>

		
	<!-- cas mobile -->

	<div class="imageMob mob"></div>

	<header class="colorTxt">  

		<p class="info">THÉÂTRE<br />samedi 27 février > 20h30</p>
		<h1>
			<a href="http://www.theatre-liberte.fr/evenements/2015-2016/33/chere-elena" target="_blank">Chère Elena</a>
		</h1>

	</header>

	<p class="logo" style="">
	  	<img src="http://waylines.fr/logogris.png" alt="titre de l'image" title="titre de l'image">
	</p>
	<p class="scrollDown"><a href="javascript:void(0)" title="Accéder aux actualités">Accéder aux actualités</a></p>

</section> <!-- fin une -->

<section class="actualite non">
	<div id="a1"></div>

	<div class="wrap">
		
		<header class="clearfix">
			<h1 class="col-sm-8 col-md-8">Prochainement</h1>

						<p class="col-sm-4 col-md-4"><a href="http://www.forumsirius.fr/orion/liberte.phtml?kld=1516&site=1516" title="Accéder à la billetterie en ligne" target="_blank"><span class="titre">Billetterie<span> en ligne</span></span><span class="icoBillet"></span></a></p>
			
		</header>

		<div class="mosSaison">
			<ul class="blocHoriz list-unstyled clearfix"> 

								  

  <li class="item rond " style="opacity:0">

    <p class="illus">
      <img src="http://www.theatre-liberte.fr/public_data/events/1453198751/sophiecalle_640.jpg" alt="sophie-callehistoires-vraies">
          </p>

        <p class="type">
                            Théma
                  </p>
    
        <p class="disc">
                            Exposition
                  </p>
    
    <p class="lieu">Hall du théâtre</p>
    <h2 class="titre">Sophie Calle - Histoires vraies</h2>
    <p class="date">du jeudi 4 février au jeudi 31 mars</p>    <p class="horaire icon-right">de 11h à 19h</p>        <p class="savoirplus">
      <a href="http://www.theatre-liberte.fr/evenements/2015-2016/532/sophie-callehistoires-vraies" title="Sophie Calle - Histoires vraies">Lire la suite</a>
    </p>

    
    <ul class="icone list-unstyled clearfix">
      
      
      
      <!-- Debut covoiturage  -->

              <li class="covoiturageIcon">

          <a  id="" class="fancyCovoiturage" title="Accéder au service de co-voiturage">
                        </a> 
        </li>
      
      <!-- Fin covoiturage -->

    </ul>
    

  </li>

  

  <li class="item rond " style="opacity:0">

    <p class="illus">
      <img src="http://www.theatre-liberte.fr/public_data/events/1452252938/eve_640.jpg" alt="eve">
          </p>

        <p class="type">
                            Théma
                  </p>
    
        <p class="disc">
                            Cinéma
                  </p>
    
    <p class="lieu">Salle Daniel Toscan du Plantier</p>
    <h2 class="titre">Ève</h2>
    <p class="date">mardi 23 février</p>    <p class="horaire icon-right">20h</p>    <div class="auteur"><p>Film de <strong>Joseph L. Mankiewicz</strong>, 1951</p></div>    <p class="savoirplus">
      <a href="http://www.theatre-liberte.fr/evenements/2015-2016/520/eve" title="Ève">Lire la suite</a>
    </p>

    
    <ul class="icone list-unstyled clearfix">
      
            <li class="billetterie"><a href="http://www.forumsirius.fr/orion/liberte.phtml?site=1516&spec=573" title="accéder à la billetterie en ligne" target="_blank"></a></li>
      
      
      <!-- Debut covoiturage  -->

              <li class="covoiturageIcon">

          <a  id="573" class="fancyCovoiturage" title="Accéder au service de co-voiturage">
                        </a> 
        </li>
      
      <!-- Fin covoiturage -->

    </ul>
    

  </li>

  

  <li class="item rond " style="opacity:0">

    <p class="illus">
      <img src="http://www.theatre-liberte.fr/public_data/events/1452253335/merechristain_640.jpg" alt="la-mere-christain">
          </p>

        <p class="type">
                            Théma
                  </p>
    
        <p class="disc">
                            Cinéma
                       |         Conférence
                  </p>
    
    <p class="lieu">Salle Daniel Toscan du Plantier</p>
    <h2 class="titre">La mère Christain</h2>
    <p class="date">jeudi 25 février</p>    <p class="horaire icon-right">20h</p>    <div class="auteur"><p>Film de <strong>Myriam Boyer</strong>, 1998<br /><br /><span>Rencontre </span><span>avec </span><strong>Myriam Boyer </strong><span>à l&rsquo;issue de la projection</span> </p></div>    <p class="savoirplus">
      <a href="http://www.theatre-liberte.fr/evenements/2015-2016/521/la-mere-christain" title="La mère Christain">Lire la suite</a>
    </p>

    
    <ul class="icone list-unstyled clearfix">
      
            <li class="billetterie"><a href="http://www.forumsirius.fr/orion/liberte.phtml?site=1516&spec=574" title="accéder à la billetterie en ligne" target="_blank"></a></li>
      
      
      <!-- Debut covoiturage  -->

              <li class="covoiturageIcon">

          <a  id="574" class="fancyCovoiturage" title="Accéder au service de co-voiturage">
                        </a> 
        </li>
      
      <!-- Fin covoiturage -->

    </ul>
    

  </li>

  

  <li class="item  " style="opacity:0">

    <p class="illus">
      <img src="http://www.theatre-liberte.fr/public_data/events/1433440481/yume_640x640.jpg" alt="yume">
          </p>

        <p class="type">
                            Spectacle
                  </p>
    
        <p class="disc">
                            Musique
                       |         Théâtre
                       |         Danse
                  </p>
    
    <p class="lieu">Salle Albert Camus</p>
    <h2 class="titre">Yumé</h2>
    <p class="date">jeudi 25 février</p>    <p class="horaire icon-right">20h30</p>    <div class="auteur"><p>D&rsquo;après la pièce de Nô <strong>Matsukazé</strong> de <strong>Kan&rsquo;Ami</strong><br />Musique <strong>Kazuko Narita</strong><br />Livret <strong>Jacques Keriguy</strong><br />Mise en scène <strong>Yoshi Oïda</strong></p></div>    <p class="savoirplus">
      <a href="http://www.theatre-liberte.fr/evenements/2015-2016/34/yume" title="Yumé">Lire la suite</a>
    </p>

    
    <ul class="icone list-unstyled clearfix">
      
            <li class="billetterie"><a href="http://www.forumsirius.fr/orion/liberte.phtml?site=1516&spec=489" title="accéder à la billetterie en ligne" target="_blank"></a></li>
      
      
      <!-- Debut covoiturage  -->

              <li class="covoiturageIcon">

          <a  id="489" class="fancyCovoiturage" title="Accéder au service de co-voiturage">
              <span class="badge">1</span>          </a> 
        </li>
      
      <!-- Fin covoiturage -->

    </ul>
    

  </li>

  

  <li class="item rond " style="opacity:0">

    <p class="illus">
      <img src="http://www.theatre-liberte.fr/public_data/events/1452677942/blogging_640.jpg" alt="blogging-et-litterature-engagement-poetique-et-politique">
          </p>

        <p class="type">
                            Théma
                  </p>
    
        <p class="disc">
                            Conférence
                  </p>
    
    <p class="lieu">Salle Daniel Toscan du Plantier</p>
    <h2 class="titre">Blogging et littérature, engagement poétique et politique</h2>
    <p class="date">vendredi 26 février</p>    <p class="horaire icon-right">20h</p>    <div class="auteur"><p>Avec <strong>Amal Claudel</strong>, blogueuse, poétesse (Tunisie), <strong>Naïla Mansour</strong>, linguiste, blogueuse (Syrie), <strong>Paola Salwan Daher</strong>, blogueuse, écrivaine (Liban/ France), <strong>Shahinaz Abdel Salam</strong> (Égypte) du projet<strong> Les Nouvelles Antigones<br /></strong>Modératrice <strong>Nil Deniz</strong></p></div>    <p class="savoirplus">
      <a href="http://www.theatre-liberte.fr/evenements/2015-2016/529/blogging-et-litterature-engagement-poetique-et-politique" title="Blogging et littérature, engagement poétique et politique">Lire la suite</a>
    </p>

    
    <ul class="icone list-unstyled clearfix">
      
            <li class="billetterie"><a href="http://www.forumsirius.fr/orion/liberte.phtml?site=1516&spec=581" title="accéder à la billetterie en ligne" target="_blank"></a></li>
      
      
      <!-- Debut covoiturage  -->

              <li class="covoiturageIcon">

          <a  id="581" class="fancyCovoiturage" title="Accéder au service de co-voiturage">
                        </a> 
        </li>
      
      <!-- Fin covoiturage -->

    </ul>
    

  </li>

  

  <li class="item  " style="opacity:0">

    <p class="illus">
      <img src="http://www.theatre-liberte.fr/public_data/events/1433440265/chereelena_640x640.jpg" alt="chere-elena">
          </p>

        <p class="type">
                            Spectacle
                  </p>
    
        <p class="disc">
                            Théâtre
                  </p>
    
    <p class="lieu">Salle Albert Camus</p>
    <h2 class="titre">Chère Elena</h2>
    <p class="date">samedi 27 février</p>    <p class="horaire icon-right">20h30</p>    <div class="auteur"><p>Texte <strong>Ludmilla Razoumovskaïa</strong><br />Traduction <strong>Joëlle</strong> et <strong>Marc Blondel</strong><br />Mise en scène <strong>Didier Long</strong></p></div>    <p class="savoirplus">
      <a href="http://www.theatre-liberte.fr/evenements/2015-2016/33/chere-elena" title="Chère Elena">Lire la suite</a>
    </p>

    
    <ul class="icone list-unstyled clearfix">
      
            <li class="billetterie"><a href="http://www.forumsirius.fr/orion/liberte.phtml?site=1516&spec=490" title="accéder à la billetterie en ligne" target="_blank"></a></li>
      
      
      <!-- Debut covoiturage  -->

              <li class="covoiturageIcon">

          <a  id="490" class="fancyCovoiturage" title="Accéder au service de co-voiturage">
                        </a> 
        </li>
      
      <!-- Fin covoiturage -->

    </ul>
    

  </li>

  

  <li class="item  screen-md" style="opacity:0">

    <p class="illus">
      <img src="http://www.theatre-liberte.fr/public_data/events/1452090842/roirene640.jpg" alt="sur-les-pas-du-roy-rene">
          </p>

        <p class="type">
                            Mardi Liberté
                  </p>
    
        <p class="disc">
                            Musique
                  </p>
    
    <p class="lieu">Hall du théâtre</p>
    <h2 class="titre">Sur les pas du Roy René</h2>
    <p class="date">mardi 1er mars</p>    <p class="horaire icon-right">12h15</p>    <div class="auteur"><p><strong>Ensemble Les Voix Animées</strong></p></div>    <p class="savoirplus">
      <a href="http://www.theatre-liberte.fr/evenements/2015-2016/518/sur-les-pas-du-roy-rene" title="Sur les pas du Roy René">Lire la suite</a>
    </p>

    
    <ul class="icone list-unstyled clearfix">
      
            <li class="billetterie"><a href="http://www.forumsirius.fr/orion/liberte.phtml?site=1516&spec=571" title="accéder à la billetterie en ligne" target="_blank"></a></li>
      
      
      <!-- Debut covoiturage  -->

              <li class="covoiturageIcon">

          <a  id="571" class="fancyCovoiturage" title="Accéder au service de co-voiturage">
                        </a> 
        </li>
      
      <!-- Fin covoiturage -->

    </ul>
    

  </li>

  

  <li class="item rond screen-md" style="opacity:0">

    <p class="illus">
      <img src="http://www.theatre-liberte.fr/public_data/events/1452253723/artemisia_640.jpg" alt="artemisia-gentileschi">
          </p>

        <p class="type">
                            Théma
                  </p>
    
        <p class="disc">
                            Cinéma
                  </p>
    
    <p class="lieu">Salle Daniel Toscan du Plantier</p>
    <h2 class="titre">Artemisia Gentileschi</h2>
    <p class="date">mardi 1er mars</p>    <p class="horaire icon-right">20h</p>    <div class="auteur"><p>Film d'<strong>Agnès Merlet</strong>, 1997</p></div>    <p class="savoirplus">
      <a href="http://www.theatre-liberte.fr/evenements/2015-2016/522/artemisia-gentileschi" title="Artemisia Gentileschi">Lire la suite</a>
    </p>

    
    <ul class="icone list-unstyled clearfix">
      
            <li class="billetterie"><a href="http://www.forumsirius.fr/orion/liberte.phtml?site=1516&spec=575" title="accéder à la billetterie en ligne" target="_blank"></a></li>
      
      
      <!-- Debut covoiturage  -->

              <li class="covoiturageIcon">

          <a  id="575" class="fancyCovoiturage" title="Accéder au service de co-voiturage">
                        </a> 
        </li>
      
      <!-- Fin covoiturage -->

    </ul>
    

  </li>

  

  <li class="item  screen-md" style="opacity:0">

    <p class="illus">
      <img src="http://www.theatre-liberte.fr/public_data/events/1433440052/enroutekaddish_640x640.jpg" alt="en-route-kaddish">
          </p>

        <p class="type">
                            Spectacle
                  </p>
    
        <p class="disc">
                            Théâtre
                  </p>
    
    <p class="lieu">Salle Fanny Ardant</p>
    <h2 class="titre">En Route-Kaddish</h2>
    <p class="date">mercredi 2 et jeudi 3 mars</p>    <p class="horaire icon-right">20h</p>    <div class="auteur"><p>Texte <strong>David Geselson</strong><br />Mise en scène et interprétation <strong>David Geselson</strong> et <strong>Elios Noël</strong></p></div>    <p class="savoirplus">
      <a href="http://www.theatre-liberte.fr/evenements/2015-2016/32/en-route-kaddish" title="En Route-Kaddish">Lire la suite</a>
    </p>

    
    <ul class="icone list-unstyled clearfix">
      
            <li class="billetterie"><a href="http://www.forumsirius.fr/orion/liberte.phtml?site=1516&spec=491" title="accéder à la billetterie en ligne" target="_blank"></a></li>
      
      
      <!-- Debut covoiturage  -->

              <li class="covoiturageIcon">

          <a  id="491" class="fancyCovoiturage" title="Accéder au service de co-voiturage">
                        </a> 
        </li>
      
      <!-- Fin covoiturage -->

    </ul>
    

  </li>

  

  <li class="item  screen-md" style="opacity:0">

    <p class="illus">
      <img src="http://www.theatre-liberte.fr/public_data/events/1433439830/trissotin.jpg" alt="trissotin-ou-les-femmes-savantes">
          </p>

        <p class="type">
                            Spectacle
                       |         Accessibilité
                  </p>
    
        <p class="disc">
                            Théâtre
                  </p>
    
    <p class="lieu">Salle Albert Camus</p>
    <h2 class="titre">Trissotin ou Les Femmes Savantes</h2>
    <p class="date">mercredi 2 et jeudi 3 mars</p>    <p class="horaire icon-right">20h30</p>    <div class="auteur"><p>D&rsquo;après <strong>Molière</strong><br />Mise en scène <strong>Macha Makeïeff</strong></p></div>    <p class="savoirplus">
      <a href="http://www.theatre-liberte.fr/evenements/2015-2016/31/trissotin-ou-les-femmes-savantes" title="Trissotin ou Les Femmes Savantes">Lire la suite</a>
    </p>

    
    <ul class="icone list-unstyled clearfix">
      
            <li class="billetterie"><a href="http://www.forumsirius.fr/orion/liberte.phtml?site=1516&spec=492" title="accéder à la billetterie en ligne" target="_blank"></a></li>
      
              <li class="vue">Accessible aux aveugles et aux malvoyants</li>
      
      <!-- Debut covoiturage  -->

              <li class="covoiturageIcon">

          <a  id="492" class="fancyCovoiturage" title="Accéder au service de co-voiturage">
                        </a> 
        </li>
      
      <!-- Fin covoiturage -->

    </ul>
    

  </li>

  



<script type='text/javascript'>

//Lightbox covoiturage
$(document).ready(function() {

  $('.fancyCovoiturage').click(function () {

    var variableId = $(this).attr('id');

    $.fancybox({
      type: 'iframe',
      href: 'http://www.theatre-liberte.fr/public_page/modules/covoiturage/covoiturage.php?idSpectacle='+variableId,
        autoSize: false,
        height: 625,
        width: 1000,
        padding : 0,
        openEffect: 'elastic',
        openSpeed : 500,
        closeEffect: 'none',
        closeSpeed : 250,
        closeBtn: false,
        helpers : {
        overlay : {
          css : {
            'background' : 'rgba(128, 128, 128, 0.5)'
          }
        }
      }
    });

  });

});

//Fin Lightbox covoiturage

 </script>


				
			</ul>  <!-- fin blocHoriz -->
		</div>  <!-- fin mosSaison -->
		
	</div> <!-- fin wrap -->
</section>  

<section class="breves non">
<h1>Brèves</h1>
	<div class="wrap">

		
		<article class="blocBrev clearfix" style="opacity:0">
			<div class="texte col-xs-12 col-sm-6 col-md-7">
				<h3 class="titre">Meursaults en tournée</h3>
				<p class="chapeau">Création Festival d'Avignon 2015</p>
				<div class="detail"><p><span style="font-size: 16px; line-height: 1.6;"><span><strong>Instituts Français d&rsquo;Algérie </strong><span>du 18 au 27 février 2016<br /><strong>Espace des Arts à Chalon-sur-Saône </strong><span>les 15 et 16 mars 2016</span></span></span><br /></span></p></div>
			</div>
			<p class="illus col-xs-12 col-sm-6 col-md-5">
									<img src="http://www.theatre-liberte.fr/public_data/news/1440407504/meursaults_tournee.jpg" alt="actualité">					
							</p>
		</article>  

		
	</div>  <!-- fin wrap -->
</section>  <!-- fin breves -->

<span id="top-link-block" class="hidden" style="z-index:99999;">
    <a href="#top" class="well well-sm">
        <i class="glyphicon glyphicon-chevron-up"></i>
    </a>
</span>

<footer class="noir non">
    <div class="wrap">
        
        <div class="dedie clearfix">
            <h4 class="col-xs-12 col-sm-12 col-md-3">Accès dédié</h4>
            <ul class="list-unstyled col-xs-12 col-sm-12 col-md-9">
                <li class="col-xs-12 col-sm-3 col-md-3"><a href="http://www.theatre-liberte.fr/vous-etes-un-groupe">Vous êtes un groupe</a></li>
                <li class="col-xs-12 col-sm-3 col-md-3"><a href="http://www.theatre-liberte.fr/vous-etes-enseignant">Vous êtes enseignant</a></li>
                <li class="col-xs-12 col-sm-3 col-md-3"><a href="http://www.theatre-liberte.fr/vous-etes-en-famille">Vous êtes en famille</a></li>
                <li class="col-xs-12 col-sm-3 col-md-3"><a href="http://www.theatre-liberte.fr/soutenir">Soutenir</a></li>
            </ul>
        </div>
        <div class="connecte clearfix">
            <h4 class="">Restons connectés</h4>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <ul class="list-unstyled col-xs-12 col-sm-6 col-md-4">
                    
                    
                    <li><a href="http://www.theatre-liberte.fr/newsletter-inscription" title="S'inscrire à notre newsletter">Newsletter</a></li>                    
                    
                    <li><a href="http://www.theatre-liberte.fr/actions-blog" title="Accéder au blog des actions">Blog des actions</a></li>     

                    <li class="reseau">
                        <a href="http://www.facebook.com/theatreliberte" class="fb" title="Accéder à notre Facebook" target="_blank">Facebook</a>
                        <a href="http://twitter.com/theatre_liberte" class="tw" title="Accéder à notre Twitter" target="_blank">Twitter</a>
                        <a href="http://vimeo.com/thliberte" class="vim" title="Accéder à notre Viméo" target="_blank">Viméo</a>
                        <a href="http://instagram.com/theatre_liberte/" class="inst" title="Accéder à notre Instagram" target="_blank">Instagram</a>
                        <a href="http://www.scoop.it/t/theatre-liberte" class="scoo" title="Accéder à notre ScoopIt" target="_blank">ScoopIt</a>
                    </li>

                </ul>
                <ul class="list-unstyled col-xs-12 col-sm-6 col-md-8 newsfb">

                    <li><a href="http://www.facebook.com/theatreliberte" title="Accéder à notre Facebook" target="_blank">Sur Facebook</a></li>
                   
                    <li>
                        <div class="breveFb" style="display:none;">
                            <ul></ul>
                        </div>
                    </li>

                </ul>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <ul class="list-unstyled col-xs-12 col-sm-6 col-md-6">
                    <li><a href="http://www.theatre-liberte.fr/public_data/saison/1433779463/brochure_theatreliberte_saison1516_.pdf" title="Télécharger la brochure de saison" target="_blank">Brochure de saison en PDF</a></li>
                                        <li><a href="http://www.forumsirius.fr/orion/liberte.phtml?kld=1516&site=1516" title="Accéder à la billetterie en ligne" target="_blank">Billetterie en ligne</a></li>
                    
                    <li><a href="http://www.theatre-liberte.fr/espace-pro" title="Accéder aux espaces pro">Espaces pro</a></li>
                    <!--<li><a href="http://www.theatre-liberte.fr/plan" title="Accéder au plan du site">Plan du site</a></li>
                    <li><a href="http://www.theatre-liberte.fr/contacts" title="Nous contacter">Contacts</a></li>-->
                    <li><a href="http://www.theatre-liberte.fr/offres-emploi-et-stages" title="Offres d'emploi et stages">Offres d'emploi et stages</a></li>
                    <li><a href="http://www.theatre-liberte.fr/credits-mentions-legales" title="Crédits et mentions légales">Crédits / mentions légales</a></li>
                </ul>

                <ul class="list-unstyled col-xs-12 col-sm-6 col-md-6 adresse">
                    <li>
                        <p><strong>Théâtre Liberté</strong><br>
                        Grand Hôtel - Place de la Liberté<br>
                        83000 Toulon<br>
                        <a href="mailto:contact@theatreliberte.fr">contact@theatreliberte.fr</a><br>
                        Administration 04 98 07 01 01<br>
                        Réservations 04 98 00 56 76
                        </p>
                    <li class="chateauvallon"><a href="http://www.chateauvallon.com" title="CNCDC Châteauvallon" target="_blank"><img class="icon" src="http://www.theatre-liberte.fr/media/public/img/simpleicon/chateauvallon.svg"> Châteauvallon</a></li>
                </ul>

            </div>
        </div>
        <div class="hashtag clearfix">
            <h4 class=""><a href="http://www.theatre-liberte.fr/instagram-tlib">#TLIB</a></h4>
        
            <div class="breveInstagramFooter" style="display:none;"></div>

        </div>
    </div>
</footer>


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <script type="text/javascript">

        var URL_MEDIA_PUBLIC_IMG = "http://www.theatre-liberte.fr/media/public/img/";
        var URL_SITE = "http://www.theatre-liberte.fr/";
        var URL_HOME= "http://www.theatre-liberte.fr/";
        var IS_NOT_DESKTOP= 0;
        </script>
        
        <script src="http://www.theatre-liberte.fr/media/public/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="http://www.theatre-liberte.fr/media/public/js/jquery.cycle2.min.js"></script>
        <script src="http://www.theatre-liberte.fr/media/public/js/jquery.cycle2.swipe.min.js" type="text/javascript"></script>
        <script src="http://www.theatre-liberte.fr/media/public/js/jquery.inview.min.js" type="text/javascript"></script>
        <script src="http://www.theatre-liberte.fr/media/public/js/jquery.scrollTo.min.js" type="text/javascript"></script>    
        <script src="http://www.theatre-liberte.fr/media/public/js/jquery-picture-min.js"></script>
        <script src="http://www.theatre-liberte.fr/media/public/js/plugins.js" type="text/javascript"></script>
        <script src="http://www.theatre-liberte.fr/media/public/js/main.js" type="text/javascript"></script>
        <script src="http://www.theatre-liberte.fr/media/public/js/imagesloaded.pkgd.min.js"></script>

        
        
<link rel="stylesheet" href="http://www.theatre-liberte.fr/media/public/css/color.css?time=1455984704">

        <script src="http://www.theatre-liberte.fr/media/public/js/script.hauteur.js" type="text/javascript"></script>        
        <script src="http://www.theatre-liberte.fr/media/public/js/jquery.waitforimages.min.js" type="text/javascript"></script>
        <script src="http://www.theatre-liberte.fr/media/public/js/script.common.js" type="text/javascript"></script>

        <script type="text/javascript">

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-36801722-1', 'auto');
  ga('send', 'pageview');

  </script>

    </body>
</html>
