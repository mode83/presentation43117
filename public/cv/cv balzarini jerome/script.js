var navbar = document.getElementById('navbar');

window.addEventListener('scroll',scrollNavbar);

function scrollNavbar() {
  if (document.documentElement.scrollTop > 315)
  {
    navbar.classList.add('nav_scroll');
  } else {
    navbar.classList.remove('nav_scroll');
  }
}

var progressbar = document.getElementById('myBar');
window.addEventListener('scroll', scrollPercent);

function scrollPercent(){
  var scroll_height = document.documentElement.scrollHeight;
  var client_height = document.documentElement.clientHeight;
  var scrollPosition = document.documentElement.scrollTop;
  var percent = (scrollPosition/(scroll_height-client_height))*100;
  progressbar.style.width = percent+"%";
};
